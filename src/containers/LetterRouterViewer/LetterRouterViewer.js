import React, { Component } from 'react';

import LetterRouter from '../../components/LetterRouter/LetterRouter'

class LetterRouterViewer extends Component {

    render() {
        return (<div>
            <LetterRouter />
        </div>)
    }
}

export default LetterRouterViewer